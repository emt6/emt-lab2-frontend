import axios from "../custom-axios/axios";

const LibraryService = {
    fetchBooks: () => {
        return axios.get("/api/books");
    },

    deleteProduct: (id) => {
        return axios.delete(`/api/books/delete/${id}`);
    },

    addBook: (name, category, author, availableCopies) => {
        return axios.post("/api/books/addNewBook", {
            "name": name,
            "category": category,
            "author": author,
            "availableCopies": availableCopies
        })
    },

    editBook: (id, name, category, author, availableCopies) => {
        return axios.put(`/api/books/${id}/edit`, {
            "name": name,
            "category": category,
            "author": author,
            "availableCopies": availableCopies
        });
    },

    markAsTaken: (id) => {
        return axios.put(`/api/books/${id}/markAsTaken`)
    },

    getProduct: (id) => {
        return axios.get(`/api/books/${id}`);
    },

};

export default LibraryService;
