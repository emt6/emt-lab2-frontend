import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Redirect, Route} from "react-router-dom";
import LibraryService from "./repository/libraryRepository";

function App() {

    return (
        <>
          <Header/>
          <Router>
            <main>
              <Route path={"/books/add"} exact render={() =>
                  <AddBook categories={this.state.categories}
                           onAddBook={this.addBook}
                  />
              }
              />
              <Route path={"/products/edit/:id"} exact render={() =>
                  <EditBook categories={this.state.categories}
                               onEditBook={this.editBook}
                               book={this.state.selectedBook}
                  />
              }
              />
              <Route path={"/books"} exact render={() =>
                  <BookList books={this.state.books}
                            onDelete={this.deleteBook}
                            onEdit={this.getBooks}
                  />
              }/>
              <Route path={"/"} exact>
                <Redirect to={"/products"}/>
              </Route>
            </main>
          </Router>
        </>
    );
}

export default App;
